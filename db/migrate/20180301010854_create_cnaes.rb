class CreateCnaes < ActiveRecord::Migration[5.1]
  def change
    create_table :cnaes do |t|
      t.string :card, limit: 20
      t.decimal :value, :precision => 13, :scale => 2
      t.string :cpf, limit: 20
      t.time :tm_occurrence
      t.date :dt_occurrence
      t.integer :tp_occurrence

      t.timestamps
    end
  end
end
