# Teste Cnae em Rails

Aplicação para teste Cnae em Rails

* Ruby version
ruby-2.5.5

* Enunciado do teste
https://gist.github.com/SauloSilva/736e38ff348b2648841af707f5110f4b

* Documentação
https://cnae-test.herokuapp.com/documentation

* Heroku link
https://cnae-test.herokuapp.com/

* Usuário e senha
email:'admin@admin.com'
password:'administrador'
