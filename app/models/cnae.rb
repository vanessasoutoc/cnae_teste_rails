class Cnae < ApplicationRecord

    #method import txt frontend
    def self.import(file)
        begin 
            File.open(file.path).each do |line|
                dt_occurrence = line.slice(1..8)
                tm_occurrence = line.slice(42..48)

                cnae = Cnae.new

                cnae.tp_occurrence = line.slice(0)

                cnae.dt_occurrence = line.slice(1..8)

                tm_occurrence = line.slice(42..48)

            # verifica se data ok e add + 3 hrs
                begin
                    testedata = DateTime.parse(dt_occurrence + tm_occurrence, '%Y%m%d%H%M%S') + (3/24.0)
                    cnae.tm_occurrence = testedata.strftime('%H:%M:%S')
                    cnae.dt_occurrence = testedata.strftime('%Y-%m-%d')
                rescue
                    cnae.tm_occurrence = nil
                    cnae.dt_occurrence = nil
                end
                
                cnae.value = line.slice(9..18)
                
                # valor dividido por 100
                cnae.value = cnae.value / 100

                cnae.cpf = line.slice(19..29)

                cnae.card = line.slice(30..41)

                # salva apenas operações de débito e crédito
                if cnae.tp_occurrence == 1 || cnae.tp_occurrence == 4
                    cnae.save
                end
            end
            return true
        rescue => ex
            logger.error ex.message
            return ex.message
        end
    end



    #method import txt api
    def self.importfile(file)
        begin 
            cnaes = Array.new
            File.open(file).each do |line|
                dt_occurrence = line.slice(1..8)
                tm_occurrence = line.slice(42..48)

                cnae = Cnae.new

                cnae.tp_occurrence = line.slice(0)

                #cnae.dt_occurrence = line.slice(1..8)

                tm_occurrence = line.slice(42..48)

                # verifica se data ok e add + 3 hrs
                begin
                    testedata = DateTime.parse(dt_occurrence + tm_occurrence, '%Y%m%d%H%M%S') + (3/24.0)
                    cnae.tm_occurrence = testedata.strftime('%H:%M:%S')
                    cnae.dt_occurrence = testedata.strftime('%Y-%m-%d')

                rescue
                    cnae.tm_occurrence = nil
                    cnae.dt_occurrence = nil
                end
                
                cnae.value = line.slice(9..18)
                
                # valor dividido por 100
                cnae.value = cnae.value / 100

                cnae.cpf = line.slice(19..29)

                cnae.card = line.slice(30..41)

                # salva apenas operações de débito e crédito
                if cnae.tp_occurrence == 1 || cnae.tp_occurrence == 4
                    cnae.save 
                    cnaes.push(cnae)
                end

            end
            return cnaes
        rescue => ex
            logger.error ex.message
            return ex.message
        end
    end


end
