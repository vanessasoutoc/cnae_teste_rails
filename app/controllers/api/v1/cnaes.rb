module API  
    module V1
        class Cnaes < Grape::API
            auth :grape_devise_token_auth, resource_class: :user
            #respond_to :json
            helpers GrapeDeviseTokenAuth::AuthHelpers

            include API::V1::Defaults

            resource :cnaes do
                desc "Return all cnaes", headers: {
                    "access-token" => {
                        description: "Valdates your identity",
                        required: true
                        },
                    "expiry" => {
                        description: "Validates your identety",
                        required: true
                        },
                    "type-token" => {
                        description: "Validates your identety",
                        required: true
                        },
                    "uid" => {
                        description: "Email login system",
                        required: true
                        },
                    "client" => {
                        description: "Validates your identety",
                        required: true
                        }
                    }
                get "", root: :cnaes do
                    authenticate_user!
                    cnaes = Cnae.all
                end

                desc "Import file Cnae format txt", headers: {
                    "access-token" => {
                        description: "Valdates your identity",
                        required: true
                        },
                    "expiry" => {
                        description: "Validates your identety",
                        required: true
                        },
                    "type-token" => {
                        description: "Validates your identety",
                        required: true
                        },
                    "uid" => {
                        description: "Email login system",
                        required: true
                        },
                    "client" => {
                        description: "Validates your identety",
                        required: true
                        },
                    "Content-Type" => {
                        description: "multpart/form-data",
                        required: true
                        }
                    }, 
                params: {
                    "cnae" => {
                        description: "Send file input",
                        required: true,
                        type: "File",
                        }
                    }
                post do
                    authenticate_user!
                    begin
                        Cnae.importfile(params[:cnae][:tempfile].path)
                    rescue
                        return {errors:[{messge: 'Arquivo não inserido.'}]}
                    end
                end

            end
        end
    end
end  
