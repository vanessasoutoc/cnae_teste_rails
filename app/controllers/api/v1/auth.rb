module API  
    module V1
        class Auth < Grape::API
            # auth :grape_devise_token_auth, resource_class: :user
            #respond_to :json
            helpers GrapeDeviseTokenAuth::AuthHelpers

            include API::V1::Defaults
            resource :auth do
                desc "Login email and password", "paths":{
                    "/auth/sign_in":{
                        "post": {
                            "summary": "Authentication by email and password",
                            "tags": [
                                "Auth"
                                ],
                            "parameters": [
                                {
                                    "name": "body",
                                    "in": "body",
                                    "description": "Model",
                                    "schema": {
                                        "$ref": "/api/v1/auth/sign_in"
                                        }
                                    }
                                ],
                            "responses": {
                                "200": {
                                    "description": "User response"
                                    },
                                "401": {
                                    "description": "Invalid login credentials"
                                    }
                                }
                            }
                        }
                    }, params: {
                        "email" => {
                            description: "Email",
                            required: true,
                            type: "string",
                            },
                        "password" => {
                            description: "Password",
                            required: true,
                            type: "string",
                            }
                        }
                post "sign_in" do
                end

            end
        end
    end
end