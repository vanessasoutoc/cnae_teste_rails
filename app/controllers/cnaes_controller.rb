class CnaesController < ApplicationController

    before_action :authenticate_user!


    def index
        @cnaes = Cnae.all
    end

    def import
        @result = Cnae.import(params[:file])
        if (@result == true)
            puts 'import'
            flash[:success] = 'Cnae importado com sucesso!'
            redirect_to cnaes_url 
        else
            if @result.index('Illegal')
                flash[:error] = 'Formato de arquivo deve ser .csv'
                redirect_to cnaes_url 
            else
                flash[:error] = @result
                redirect_to cnaes_url    
            end

        end
    end

end
