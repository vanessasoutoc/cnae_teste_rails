class CnaeSerializer < ActiveModel::Serializer
  attributes :id, :value, :card, :tm_occurrence, :dt_occurrence, :cpf, :tp_occurrence
end
