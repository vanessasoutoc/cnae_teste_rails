Rails.application.routes.draw do
    devise_for :users, :skip => :registration

    namespace :api do
        scope :v1 do
            mount_devise_token_auth_for 'User', at: 'auth'
        end
    end

    mount GrapeSwaggerRails::Engine, at: "/documentation"


    mount API::Base, at: "/"

    resources :cnaes
    post 'import_cnae' =>'cnaes#import', as: :import_cnae

    root to: 'cnaes#index'
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
