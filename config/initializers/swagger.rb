GrapeSwaggerRails.options.app_url = Rails.env == "production" ? "https://cnae-test.herokuapp.com" : "http://localhost:3000"

GrapeSwaggerRails.options.app_name = 'Cnae Teste Rails'
GrapeSwaggerRails.options.url = '/api/v1/swagger_doc'